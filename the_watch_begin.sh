LOG=$(cat /var/log/crontab.log)
CURRENT=$(stat -c %y /etc/crontab)
if [ "$LOG" != "$CURRENT" ]
then
	echo "LE CORRECTEUR A MODIFIER TON CRONTAB !" | mail -s 'Crontab modification' root
else
	stat -c %y /etc/crontab > /var/log/crontab.log
fi
