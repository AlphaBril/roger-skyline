echo "=== Starting Update ===" >> /var/log/update_script.log
date >> /var/log/update_script.log
echo "=== Release the KRAKEN ===" >> /var/log/update_script.log
/sbin/iptables -F
/sbin/iptables -X
/sbin/iptables -P INPUT ACCEPT
/sbin/iptables -P OUTPUT ACCEPT
/sbin/iptables -P FORWARD ACCEPT
echo "=== apt-get update ===" >> /var/log/update_script.log
wait
apt-get update -y >> /var/log/update_script.log
echo "=== apt-get upgrade ===" >> /var/log/update_script.log
apt-get upgrade >> /var/log/update_script.log
echo "=== Mission failed.. Back up ! ===" >> /var/log/update_script.log
sh /bin/firewall.sh
echo "=== End of Update ===\n\n\n" >> /var/log/update_script.log
