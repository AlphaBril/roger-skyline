apt-get install -y mailutils
echo "Port 42" >> /etc/ssh/sshd_config
echo "PubkeyAuthentication yes" >> /etc/ssh/sshd_config
echo "PermitRootLogin no" >> /etc/ssh/sshd_config
echo "RSAAuthentication yes" >> /etc/ssh/sshd_config
echo "# This file describes the network interfaces available on your system\n# and how to activate them. For more information, see interfaces(5).\n\nsource /etc/network/interfaces.d/*\n# The loopback network interface\nauto lo\niface lo inet loopback\n\n# The primary network interface\nauto enp0s3\niface enp0s3 inet static\n\taddress 10.11.42.42\n\tnetmask 255.255.255.252\n\tgateway 10.11.254.254" > /etc/network/interfaces
echo "nameserver 8.8.8.8" >> /etc/resolv.conf
mkdir /home/roger/.ssh
touch /home/roger/.ssh/authorized_keys
echo "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDZ+Cn73F+8tDz0s0lrCV+wOr2U6G/x/xMwEdMg7CYFmLCoWNsrbiQ3khH/xXzZhmQSzjBuMMR+OTP/HXePKhV94DhOGWJZ2t/z5VRPu4IYGQSa/FIFq01zGAL7CcnGnz1c1H8YSunFpd6WsKrX4VdcvAqUa6ADO/upcsBBYcY5umFLLHB/Wr3BuExeO0CJ2d3e6DpMbqg5R3iDZmSmQe9wM1G16bnSawZwAlHbJ4IxPGYnVkpWaDUQbOaAt9O928Hn7Ys1wBlc5KvABfYnMIRSFhLfKmbwSZTLArHBpnj9B/dp3HE4Bajgq3nlu0hGju5eMg9FEcnUbDTapSlhNLR9 fldoucet@e1r7p20.42.fr" > /home/roger/.ssh/authorized_keys
rm -rf /var/mail/*
touch /var/log/update_script.log
