<?php
session_start();
if (!isset($_SESSION['pseudo'])) {
	header ('Location: index.php');
	exit();
}
?>
<!DOCTYPE html>
<html>
    <head>
	<meta charset="utf-8" />
	<title>42 Squad Chat</title>
	<link rel="stylesheet" type="text/css" href="design.css">
    </head>
    <style>
    form
    {
	text-align:center;
    }
    </style>
    <body>

	<div class="form">
	    <form class="login-form" action="minichat_post.php" method="post">
		<p>
		<h1>Bonjour <?php echo $_SESSION['pseudo']; ?></h1>
		<input type="hidden" name="pseudo" id="pseudo" value="<?php echo $_SESSION['pseudo']; ?>"/><br />
		<input type="text" placeholder="message" name="message" id="message" />
		<button type="submit" value="Envoyer">envoyer</button>
		<p class="message"><a href="logout.php">Logout</a></p>
	    </form>
	</div>

<?php
try
{
	$bdd = new PDO('mysql:host=localhost;dbname=mini-chat;charset=utf8', 'db', 'db');
}
catch(Exception $e)
{
	die('Erreur : '.$e->getMessage());
}
$reponse = $bdd->query('SELECT pseudo, message FROM Message ORDER BY ID DESC LIMIT 0, 10');
echo '<div class="formchat">';
while ($donnees = $reponse->fetch())
{
	echo '<div style="width: 350px; overflow-wrap: break-word"><p><strong>' . htmlspecialchars($donnees['pseudo']) . '</strong> : ' . htmlspecialchars($donnees['message']) . '</div></p>';
}
echo '</div>';
$reponse->closeCursor();
?>
    </body>
</html>
