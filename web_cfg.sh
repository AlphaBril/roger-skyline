apt-get install libapache2-mod-php mysql-server php-mysql phpmyadmin
cp ./site.conf /etc/apache2/sites-available/site.conf
cp ./site-ssl.conf /etc/apache2/sites-available/site-ssl.conf
mkdir /etc/apache2/ssl
openssl req -x509 -nodes -days 365 -newkey rsa:2048 -keyout /etc/apache2/ssl/apache.key -out /etc/apache2/ssl/apache.crt
cp -R ./Site /var/www/html/
a2enmod ssl
a2enmod php5
a2ensite site.conf
a2ensite site-ssl.conf
service apache2 restart
