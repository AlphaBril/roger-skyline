# === Firewall ===

# On supprime toute les regles
/sbin/iptables -F
/sbin/iptables -X

# On met toutes les polices en refus
/sbin/iptables -P INPUT DROP
/sbin/iptables -P OUTPUT DROP
/sbin/iptables -P FORWARD DROP

# On accepte le tcp sur le port 42 (ssh)
/sbin/iptables -A INPUT -p tcp --dport 42 -j ACCEPT
/sbin/iptables -A OUTPUT -p tcp --sport 42 -j ACCEPT

# On accepte le tcp sur le port 80 redirection 443 (htmls)
/sbin/iptables -A INPUT -p tcp --dport 80:443 -j ACCEPT
/sbin/iptables -A OUTPUT -p tcp --sport 80:443 -j ACCEPT

# On s'assure de ne rien accepter d'autre
/sbin/iptables -A INPUT -j DROP
/sbin/iptables -A OUTPUT -j DROP

# === Anti-DOS ===

# On interdit l'usupation d'IP
/sbin/iptables -A INPUT -s 10.0.0.0/8 -j DROP
/sbin/iptables -A INPUT -s 169.254.0.0/16 -j DROP
/sbin/iptables -A INPUT -s 172.16.0.0/12 -j DROP
/sbin/iptables -A INPUT -i enp0s3 -s 127.0.0.0/8 -j DROP

/sbin/iptables -A INPUT -s 224.0.0.0/4 -j DROP
/sbin/iptables -A INPUT -d 224.0.0.0/4 -j DROP
/sbin/iptables -A INPUT -s 240.0.0.0/5 -j DROP
/sbin/iptables -A INPUT -d 240.0.0.0/5 -j DROP
/sbin/iptables -A INPUT -s 0.0.0.0/8 -j DROP
/sbin/iptables -A INPUT -d 0.0.0.0/8 -j DROP
/sbin/iptables -A INPUT -d 239.255.255.0/24 -j DROP
/sbin/iptables -A INPUT -d 255.255.255.255 -j DROP

# On interdit les paquets invalides
/sbin/iptables -A INPUT -m state --state INVALID -j DROP
/sbin/iptables -A FORWARD -m state --state INVALID -j DROP
/sbin/iptables -A OUTPUT -m state --state INVALID -j DROP

# Si trop de paquets RST s'enchaine, on interdit (smurf attaque)
/sbin/iptables -A INPUT -p tcp -m tcp --tcp-flags RST RST -m limit --limit 2/second --limit-burst 2 -j ACCEPT

# === Anti-SCAN ===

# Quicquonque tente un scan de port est bloquer pour 1 jour
/sbin/iptables -A INPUT   -m recent --name portscan --rcheck --seconds 86400 -j DROP
/sbin/iptables -A FORWARD -m recent --name portscan --rcheck --seconds 86400 -j DROP

# Une fois un jours passer, on les retire de la liste
/sbin/iptables -A INPUT   -m recent --name portscan --remove
/sbin/iptables -A FORWARD -m recent --name portscan --remove

# Ajoute les scanneurs a la liste et ecrit le tout dans les logs avec prefix
/sbin/iptables -A INPUT   -p tcp -m tcp --dport 139 -m recent --name portscan --set -j LOG --log-prefix "Portscan:"
/sbin/iptables -A INPUT   -p tcp -m tcp --dport 139 -m recent --name portscan --set -j DROP

/sbin/iptables -A FORWARD -p tcp -m tcp --dport 139 -m recent --name portscan --set -j LOG --log-prefix "Portscan:"
/sbin/iptables -A FORWARD -p tcp -m tcp --dport 139 -m recent --name portscan --set -j DROP
